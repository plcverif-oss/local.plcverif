This project is not part of the PLCverif release. It is a simple helper project to be included into the Eclipse's workspace in order to launch PLCverif within Eclipse. It is only intended for development and debugging.

## HOWTO

* import the project into the workspace
* open the file `releng/local.plcverif.target/local.plcverif.target`
* click on `Set Active Platform`  or `Reload Target Platform` at the top right corner